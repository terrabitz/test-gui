function Import-Xaml {
    [CmdletBinding()]
    param (
        # The path to the XAML file
        [Parameter(Mandatory)]
        [string]
        $XamlFile,

        # The prefix to add to all WPF named controls
        [Parameter(Mandatory = $false)]
        [string]
        $Prefix = "WPF"
    )

    begin {
    }

    process {
        $inputXML = Get-Content $XamlFile
        $inputXML = $inputXML -replace 'mc:Ignorable="d"', '' -replace "x:N", 'N' -replace '^<Win.*', '<Window'
        [xml]$XAML = $inputXML

        $reader = (New-Object System.Xml.XmlNodeReader $xaml)
        try {
            $Form = [Windows.Markup.XamlReader]::Load( $reader )
        } catch {
            Write-Warning "Unable to parse XML, with error: $($Error[0])`n Ensure that there are NO SelectionChanged or TextChanged properties in your textboxes (PowerShell cannot process them)"
            throw
        }

        #===========================================================================
        # Load XAML Objects In PowerShell
        #===========================================================================

        foreach ($node in $xaml.SelectNodes("//*[@Name]")) {
            Write-Verbose "trying item $($node.Name)"
            try {
                Set-Variable -Name "$($Prefix)$($node.Name)" -Value $Form.FindName($node.Name) -ErrorAction Stop
            } catch {
                throw
            }
        }
        $Form
    }

    end {
    }
}