function Invoke-TestGui {
    [CmdletBinding()]
    param (

    )

    begin {
    }

    process {
        $xamlPath = Join-Path $etcPath "main.xaml"
        $form = Import-Xaml $xamlPath

        $form.ShowDialog() | Out-Null
    }

    end {
    }
}