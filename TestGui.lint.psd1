@{
    IncludeDefaultRules = $True
    ExcludeRules        = @('PSAvoidGlobalVars', 'PSShouldProcess')
    Rules               = @{
        PSPlaceOpenBrace           = @{
            Enable             = $True
            OnSameLine         = $True
            NewLineAfter       = $True
            IgnoreOneLineBlock = $True
        }

        PSPlaceCloseBrace          = @{
            Enable             = $True
            NewLineAfter       = $False
            IgnoreOneLineBlock = $True
            NoEmptyLineBefore  = $True
        }

        PSUseConsistentIndentation = @{
            Enable          = $False
            Kind            = 'space'
            IndentationSize = 4
        }

        PSUseConsistentWhitespace  = @{
            Enable         = $True
            CheckOpenBrace = $False
            CheckOpenParen = $True
            CheckOperator  = $False
            CheckSeparator = $True
        }

        PSAlignAssignmentStatement = @{
            Enable         = $True
            CheckHashtable = $True
        }
    }
}