# TestGui

A simple test GUI using WPF plus PowerShell

## Next steps

1. Place all publicly-exposed functions into `TestGui\Public`
2. Place all private functions into `TestGui\Private`
