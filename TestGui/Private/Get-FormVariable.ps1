function Get-FormVariable {
    [CmdletBinding()]
    param (
        # The prefix for all form controls
        [Parameter(Mandatory = $false)]
        [string]
        $Prefix = "WPF"
    )

    begin {
    }

    process {
        Get-Variable "$($Prefix)*"
    }

    end {
    }
}